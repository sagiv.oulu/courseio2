from pydantic import BaseSettings, SecretStr


class Settings(BaseSettings):
    log_level: str = ['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL']
    mongo_hostname: str
    mongo_port: int
    mongo_username: str
    mongo_password: SecretStr
