import logging
from typing import List, Type
import re
from bson.objectid import ObjectId
import hashlib

from fastapi import FastAPI, HTTPException
from pydantic import BaseModel, Field
from mongoengine import connect
import mongoengine

from settings import Settings
from open_uni import get_courses as scrape_courses_from_uni, Course as ScrapedCourse
from open_uni_fields import (
    scrape_departments,
    scrape_sub_departments,
    scrape_sub_department_programs,
    scrape_program_courses_ids
)
import database as db
from database import (
    Course as DbCourse,
    Department as DbDepartment,
    SubDepartment as DbSubDepartment,
    Program as DbProgram,
    ProgramCourse as DbProgramCourse
)


class Course(BaseModel):
    id: int
    name: str
    points: int
    subjects: List[str]
    association: str
    requirements: str

    @staticmethod
    def from_mongo_engine(db_course: DbCourse) -> 'Course':
        return Course(id=db_course.course_id,
                      name=db_course.name,
                      points=db_course.points,
                      subjects=db_course.subjects,
                      association=db_course.association,
                      requirements=db_course.requirements)

    class Config:
        orm_mode = True


class Department(BaseModel):
    id: str
    name: str


class SubDepartment(BaseModel):
    id: str
    department_id: str
    link: str


class Program(BaseModel):
    id: str
    name: str
    sub_department_id: str
    department_id: str
    link: str

    @staticmethod
    def from_mongo_engine(db_program: DbProgram) -> 'Program':
        return Program(id=db_program.program_id,
                       name=db_program.name,
                       sub_department_id=db_program.sub_department_id,
                       department_id=db_program.department_id,
                       link=db_program.link)


class ExpandedProgram(Program):
    courses: List[Course]


def get_app(settings: Settings) -> FastAPI:

    app = FastAPI(title='courseio2')

    @app.on_event("startup")
    def startup_event():
        def create_object_id(identifier):
            return ObjectId(hashlib.md5(str(identifier).encode('utf-8')).hexdigest()[:24])

        connect('admin',
                host=settings.mongo_hostname,
                port=settings.mongo_port,
                username=settings.mongo_username,
                password=settings.mongo_password.get_secret_value())

        courses_already_in_db = list(DbCourse.objects)
        if not courses_already_in_db:
            logging.info('Database has no courses, scraping & saving them...')
            scraped_courses: List[ScrapedCourse] = scrape_courses_from_uni(only_complete_information=True)
            for scraped_course in scraped_courses:
                dbCourse = DbCourse(
                    _id=create_object_id(scraped_course.id),
                    course_id=scraped_course.id,
                    name=scraped_course.name,
                    points=scraped_course.points,
                    subjects=scraped_course.subjects,
                    association=scraped_course.association,
                    link=scraped_course.link,
                    requirements=scraped_course.requirements)
            
                dbCourse.save()
        
        departments_already_in_db = list(db.Department.objects)

        if not departments_already_in_db:
            logging.info('Database has no departments, scraping & saving them...')
            scraped_departments = list(scrape_departments())
            for scraped_department in scraped_departments:
                db_department = db.Department(
                    _id=create_object_id(scraped_department.id),
                    department_id=scraped_department.id,
                    name=scraped_department.name)
            
                db_department.save()
        
        sub_departments_already_in_db = list(db.SubDepartment.objects)

        if not sub_departments_already_in_db:
            logging.info('Database has no subdepartments, scraping & saving them')
            scraped_sub_departments = list(scrape_sub_departments(scraped_departments))
            for scraped_sub_department in scraped_sub_departments:
                db_sub_department = db.SubDepartment(
                    _id=create_object_id(scraped_sub_department.id),
                    sub_department_id=scraped_sub_department.id,
                    department_id=scraped_sub_department.department_id,
                    link=scraped_sub_department.link)
            
                logging.info('saving sub department %s - %s', db_sub_department.department_id, db_sub_department.sub_department_id)
                db_sub_department.save()
        
        programs_already_in_db = list(db.Program.objects)

        if not programs_already_in_db:
            logging.info('Database has no sub department programs, scraping & saving them')
            for scraped_sub_department in scraped_sub_departments:
                scraped_programs = scrape_sub_department_programs(scraped_sub_department)
                for scraped_program in scraped_programs:
                    logging.info('saving program %s - %s - %s', scraped_program.department_id, scraped_program.sub_department_id, scraped_program.id)
                    db_program = db.Program(
                        _id=create_object_id(scraped_program.id),
                        program_id=scraped_program.id,
                        name=scraped_program.name,
                        sub_department_id=scraped_program.sub_department_id,
                        department_id=scraped_program.department_id,
                        link=scraped_program.link)
            
                    db_program.save()
            
                    courses_ids = scrape_program_courses_ids(scraped_program)
                    for course_id in courses_ids:
                        db_program_course = db.ProgramCourse(
                            _id=create_object_id(f'{course_id}-{scraped_program.id}'),
                            course_id=course_id,
                            program_id=scraped_program.id)
            
                        db_program_course.save()
        
        logging.info('Startup data collection complete')

    @app.get('/courses', response_model=List[Course])
    def get_courses():
        return [Course.from_orm(dbCourse) for dbCourse in DbCourse.objects]

    @app.get('/departments', response_model=List[Department])
    def get_departments():
        return [Department(id=db_department.department_id,
                           name=db_department.name) for db_department in DbDepartment.objects]

    @app.get('/subdepartments', response_model=List[SubDepartment])
    def get_sub_department():
        return [SubDepartment(id=db_sub_department.sub_department_id,
                              department_id=db_sub_department.department_id,
                              link=db_sub_department.link) for db_sub_department in DbSubDepartment.objects]

    @app.get('/programs', response_model=List[Program])
    def get_programs():
        return [Program.from_mongo_engine(db_program) for db_program in DbProgram.objects]

    @app.get('/programs/{program_id}', response_model=Program)
    def get_programs(program_id: str):
        db_programs = list(DbProgram.objects(program_id=program_id))
        if len(db_programs) == 0:
            raise HTTPException(status_code=404, detail="Program not found")

        if len(db_programs) > 1:
            raise HTTPException(status_code=500,
                                detail=f"Found multiple {len(db_programs)} programs matching with the id {program_id}")

        db_program = db_programs[0]
        return Program.from_mongo_engine(db_program)

    @app.get('/programs/{program_id}/courses', response_model=List[Course])
    def get_programs(program_id: str):
        db_programs = list(DbProgram.objects(program_id=program_id))
        if len(db_programs) == 0:
            raise HTTPException(status_code=404, detail="Program not found")

        if len(db_programs) > 1:
            raise HTTPException(status_code=500,
                                detail=f"Found multiple {len(db_programs)} programs matching with the id {program_id}")

        db_program = db_programs[0]
        program = Program.from_mongo_engine(db_program)

        db_program_courses = DbProgramCourse.objects(program_id=program.id)
        courses_ids = list(map(lambda db_program_course: db_program_course.course_id, db_program_courses))

        db_courses = DbCourse.objects(course_id__in=courses_ids)

        courses = [Course.from_mongo_engine(db_course) for db_course in db_courses]
        return courses

    return app


settings = Settings()
logging.basicConfig(level=getattr(logging, settings.log_level.upper()))
app = get_app(settings)
