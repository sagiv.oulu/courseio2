import re
from multiprocessing.dummy import Pool
from datetime import datetime
from typing import List
from functools import partial
from pprint import pprint

from pydantic import BaseModel
import requests
from bs4 import BeautifulSoup
import pandas as pd


class Course(BaseModel):
    id: str
    name: str
    link: str
    points: int = None

    subjects: List[str]

    association: str = None

    # HTML contents of the requirements of the course (including a tags & all)
    raw_requirements_html: str = None

    # The raw text of the courses requirements, straight from the course page
    raw_requirements_text: str = None

    # Course requirements, but every link is replaced with the classification of course / subject + the assosiated id
    requirements_text_with_ids: str = None

    # The previous requirements part of the requirements text with ids field
    previous_required_knowledge: str = None

    # The acceptance conditions part of the requirements text with ids field
    acceptance_conditions: str = None

    # Formalization of the previous_required_knowledge text
    formal_previous_required_knowledge: str = None

    # Formalization of the acceptance_conditions text
    formal_acceptance_conditions: str = None

    requirements: str = None


def get_courses_page_rows(page: int):
    _page = requests.get(f'https://www3.openu.ac.il/ouweb/owal/catalog.sknum?v_last={page}&v_machlaka=').content
    _page = BeautifulSoup(_page, 'html.parser')

    # Get the table containing the list of courses
    tables = _page.select('.table1')
    table = tables[0]

    # Get all rows of the table, each row contains a single course information
    rows = table.select('tr')
    if len(rows) > 1:
        rows = rows[1:]
    else:
        rows = []

    return rows


def get_all_courses_rows():
    rows = []
    for pagination in range(1, 1024):
        _rows = get_courses_page_rows(pagination)
        if not _rows:
            break

        rows = rows + _rows

    return rows


def get_course_row_info(row):
    cells = row.select('td')
    # First cell: Course name & list
    # Second cell: course level
    # Third cell: course points
    # Forth cell: Is the course offered abroad
    # Fifth cell: Semesters the course is available in

    # Get the course link from the table row
    course_link = cells[0].select_one('a')['href']

    # Get course name, & remove the course id from the name
    course_name = cells[0].select_one('a').text
    course_name = re.sub(r'\(\d+\)', '', course_name).strip()

    # Get the course id from the link
    course_id_matches = re.findall('mkurs=(\d+)', course_link)
    if len(course_id_matches) != 1:
        raise Exception(f'Unexpected course link: {course_link}')
    course_id = course_id_matches[0]

    course_points_text = cells[2].text.strip()

    return course_id, course_name, course_link, course_points_text


def get_course_page(course_id: int):
    return requests.get(f'https://www.openu.ac.il/courses/{course_id}.htm').content


def get_course_requirements_paragraph(course_page):
    _course_page = BeautifulSoup(course_page, 'html.parser')
    paragraphs = _course_page.select('p:contains("ידע קודם דרוש:"), '
                                     'p:contains("ידע דרוש:"), '
                                     'p:contains("ידע מוקדם:"), '
                                     'p:contains("ידע קודם:")')
    if len(paragraphs) == 1:
        return paragraphs[0]
    return None


def get_course_association(course_page) -> str:
    _course_page = BeautifulSoup(course_page, 'html.parser')
    associations = _course_page.select('p:contains("שיוך:")')
    if len(associations) == 1:
        return associations[0].text.replace('שיוך: ', '').strip()

    return None


def parse_course_points_text(points_text: str):
    points = points_text
    if points == '':
        points = '0'
    number_matches = re.findall(r'(\d+)', points)
    found_points = len(number_matches) == 1

    if found_points:
        points = int(number_matches[0])
    else:
        points = None

    return points


def replace_a_tag(a_tag):
    link = a_tag['href']

    # Check if the link is a course link
    course_matches = re.findall(r'https?\:\/\/www\.openu\.ac\.il\/courses\/(\d+)\.htm', link)
    if len(course_matches) == 1:
        _course_id = course_matches[0]
        a_tag.string = f'course_{_course_id}'

    # Check if the link is a course link (there is a different kind of link that is also a course link)
    course_matches = re.findall(
        r'file\:\/\/\/\\\\openu\.local\\sharing\\_GROUP\\yedeon\\YedionTamplates\\SingleCourseDescription\\(\d+)',
        link)
    if len(course_matches) == 1:
        _course_id = course_matches[0]
        a_tag.string = f'course_{_course_id}'

    # Check if the link is a subject link
    subject_matches = re.findall(
        f'https?\:\/\/www3\.openu\.ac\.il\/ouweb\/owal\/catalog\.sksub_list\?v_kod_nose=(\d+)',
        link)
    if len(subject_matches) == 1:
        subject_id = subject_matches[0]
        a_tag.string = f'subject_{subject_id}'

    a_tag.unwrap()


# Load formalization table (key = informal expression, value = formal expression)
_formalizations = pd.read_excel('./requirements_formalizations.xlsx').to_dict('records')
formalizations = {}
for f in _formalizations:
    if isinstance(f['formal'], str):
        formalizations[f['informal']] = f['formal']


def formalize(text: str):
    if not text:
        return 'true'

    formalized = None
    format = re.sub(r'course_\d+', 'course', text)
    format = re.sub(r'subject_\d+', 'subject', format)
    format = re.sub(r'\d+', 'number', format)
    formalization = formalizations.get(format)
    if formalization:
        format = re.escape(format)
        format = re.sub('course', r'(course_\\d+)', format)
        format = re.sub('subject', r'(subject_\\d+)', format)
        format = re.sub('number', r'(\\d+)', format)
        formalized = re.sub(format, formalization, text)

    return formalized


def extract_previous_required_knowledge(requirements: str) -> str:
    previous_required_knowledge_matches = re.findall(r'ידע קודם דרוש:([^.]*?)\.', requirements)
    if len(previous_required_knowledge_matches) == 1:
        previous_required_knowledge = previous_required_knowledge_matches[0].strip()
        return previous_required_knowledge

    return ''


def extract_acceptance_conditions(requirements: str) -> str:
    acceptance_conditions_matches = re.findall(r'תנאי קבלה:([^.]*?)\.', requirements)
    if len(acceptance_conditions_matches) == 1:
        acceptance_conditions = acceptance_conditions_matches[0].strip()
        return acceptance_conditions

    return ''


def get_row_course(row, courses_subjects_map: dict) -> Course:
    course_id, course_name, course_link, course_points_text = get_course_row_info(row)
    course_subjects = courses_subjects_map.get(course_id, [])

    course = Course(id=course_id, name=course_name, link=course_link, subjects=course_subjects)

    try:
        # If the course points field contains irregular text (more than one number) skip this course
        course.points = parse_course_points_text(course_points_text)

        if course.points is not None:
            course_page = get_course_page(course.id)
            course.association = get_course_association(course_page)
            requirements = get_course_requirements_paragraph(course_page)
            if requirements:

                # Remove all the spans (little numbers next to the course name)
                spans = requirements.select('span')
                for span in spans:
                    span.string = ''
                    span.unwrap()

                course.raw_requirements_text = requirements.text.strip()
                course.raw_requirements_html = str(requirements)

                # Replace all links with the link url
                a_tags = requirements.select('a')
                for a_tag in a_tags:
                    replace_a_tag(a_tag)

                # Now that we replaced the course links with course link urls, we can work with the text directly
                requirements = requirements.text.strip()

                # Remove weird special characters
                requirements = requirements.replace((b'\xe2\x80\x8f').decode('utf-8'), '')
                requirements = requirements.replace((b'\xe2\x80\x8e').decode('utf-8'), '')
                requirements = requirements.replace((b'\xef\xbb\xbf').decode('utf-8'), '')

                # Remove extra course numbers in text
                requirements = re.sub(r'\(\d+\)', '', requirements)

                # Replace multiple spaces with single spaces & strip spaces from end & start
                requirements = re.sub(r'\s+', ' ', requirements)
                requirements = requirements.strip()

            course.requirements_text_with_ids = requirements if requirements else ''

            course.previous_required_knowledge = extract_previous_required_knowledge(course.requirements_text_with_ids)
            course.acceptance_conditions = extract_acceptance_conditions(course.requirements_text_with_ids)

            course.formal_previous_required_knowledge = formalize(course.previous_required_knowledge)
            course.formal_acceptance_conditions = formalize(course.acceptance_conditions)

            if course.formal_acceptance_conditions and course.formal_previous_required_knowledge:
                course.requirements = f'({course.formal_previous_required_knowledge}) and ' \
                                      f'({course.formal_acceptance_conditions})'

    except Exception as e:
        print(f'Caught an error parsing course {course.name} ({course.id}) - {e}')

    return course


def get_subject_courses_table_rows(subject_id: int):
    page = requests.get(f'https://www3.openu.ac.il/ouweb/owal/catalog.sksub_list?v_kod_nose={subject_id}').content
    page = BeautifulSoup(page, 'html.parser')

    # Get the table containing the list of courses
    tables = page.select('.table1')
    table = tables[0]

    # Get all rows of the table, each row contains a single course information
    rows = table.select('tr')
    if len(rows) > 1:
        rows = rows[1:]
    else:
        rows = []

    return rows


def get_courses(only_complete_information: bool = False):
    rows = get_all_courses_rows()
    courses_subjects_map = get_courses_subjects()
    get_row_course_partial = partial(get_row_course, courses_subjects_map=courses_subjects_map)

    start_time = datetime.now()
    pool = Pool(processes=200)
    courses = pool.map(get_row_course_partial, rows)
    end_time = datetime.now()
    print(f'Got all courses from rows, took {end_time - start_time}')

    if only_complete_information:
        courses = list(filter(lambda course: None not in [course.points, course.association, course.requirements],
                              courses))

    return courses


def get_courses_subjects():
    page = requests.get('https://www3.openu.ac.il/ouweb/owal/catalog.sksub').content
    page = BeautifulSoup(page, 'html.parser')

    general_association_tables = page.select('.list_sub')
    general_association_headers = page.select('.sub_top, .sub_top2')

    subjects_ids = []

    course_subjects_map = {}

    for subjects_table, general_subject_title in zip(general_association_tables, general_association_headers):
        general_subject_name = general_subject_title.text
        subject_a_tags = subjects_table.select('a')

        print(f'Getting general subject {general_subject_name} subjects')

        for subject_a_tag in subject_a_tags:
            subject_name = subject_a_tag.text
            subject_id = re.findall(r'/ouweb/owal/catalog\.sksub_list\?v_kod_nose=(\d+)', subject_a_tag['href'])[0]
            subjects_ids.append(subject_id)

            print(f'Getting courses in {general_subject_name} - {subject_name}')

            rows = get_subject_courses_table_rows(subject_id)

            for row in rows:
                course_id, course_name, course_link, course_points_text = get_course_row_info(row)
                course_subjects_map[course_id] = course_subjects_map.get(course_id, []) + [subject_id]

    def get_subject_id_course_ids(subject_id: int):
        rows = get_subject_courses_table_rows(subject_id)
        courses_ids = []

        for row in rows:
            course_id, course_name, course_link, course_points_text = get_course_row_info(row)
            courses_ids.append(course_id)

        return courses_ids

    return course_subjects_map


def is_course_data_complete(course: Course) -> bool:
    return course.id is not None and \
           course.name is not None and \
           course.link is not None and \
           course.points is not None and \
           course.raw_requirements_html is not None and \
           course.raw_requirements_text is not None and \
           course.requirements_text_with_ids is not None and \
           course.previous_required_knowledge is not None and \
           course.acceptance_conditions is not None and \
           course.formal_previous_required_knowledge is not None and \
           course.formal_acceptance_conditions is not None


def does_course_have_basic_info(course: Course) -> bool:
    return course.id is not None and \
           course.name is not None and \
           course.link is not None and \
           course.points is not None


def does_course_have_all_requirements_info(course: Course) -> bool:
    return course.id is not None and \
           course.name is not None and \
           course.link is not None and \
           course.points is not None and \
           course.raw_requirements_html is not None and \
           course.raw_requirements_text is not None and \
           course.requirements_text_with_ids is not None and \
           course.previous_required_knowledge is not None and \
           course.acceptance_conditions is not None


if __name__ == '__main__':
    print('getting all courses')
    courses = get_courses()
    print(f'got {len(courses)} courses')

    courses_with_basic_info = list(filter(does_course_have_basic_info, courses))
    print(f'{len(courses_with_basic_info)} / {len(courses)} courses have basic info')

    courses_with_requirements_ids_text = list(filter(lambda course: does_course_have_basic_info(course) and course.requirements_text_with_ids is not None, courses))
    print(f'{len(courses_with_basic_info)} / {len(courses)} courses have basic info and requirements ids text')

    course_with_all_requirements_info = list(filter(does_course_have_all_requirements_info, courses))
    print(f'{len(courses_with_basic_info)} / {len(courses)} courses have all requirements info')

    courses_with_complete_data = list(filter(is_course_data_complete, courses))
    print(f'{len(courses_with_complete_data)} / {len(courses)} courses have complete data')

    df = pd.DataFrame(list(map(BaseModel.dict, courses)))
    print(df)
    df.to_excel("output.xlsx")
