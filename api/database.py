from mongoengine import Document, StringField, ListField, ObjectIdField, IntField


class Course(Document):
    _id = ObjectIdField(required=True)
    course_id = StringField(required=True, unique=True)
    name = StringField(required=True)
    link = StringField(required=True)
    points = IntField(required=True)
    subjects = ListField(IntField())
    association = StringField()
    raw_requirements_text = StringField()
    requirements = StringField(required=True)


class Department(Document):
    _id = ObjectIdField(required=True)
    department_id = StringField(required=True, unique=True)
    name = StringField(required=True)


class SubDepartment(Document):
    _id = ObjectIdField(required=True)
    sub_department_id = StringField(required=True, unique=True)
    department_id = StringField(required=True)
    link = StringField(required=True)


class Program(Document):
    _id = ObjectIdField(required=True)
    program_id = StringField(required=True, unique=True)
    name = StringField(required=True)
    sub_department_id = StringField(required=True)
    department_id = StringField(required=True)
    link = StringField(required=True)


class ProgramCourse(Document):
    _id = ObjectIdField(required=True)
    course_id = StringField(required=True)
    program_id = StringField(required=True)
