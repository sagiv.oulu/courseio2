import re
from multiprocessing.dummy import Pool
from typing import Iterable
from functools import reduce
from itertools import chain
import logging
from functools import lru_cache

from pydantic import BaseModel
import requests
from bs4 import BeautifulSoup


class Department(BaseModel):
    id: str
    name: str


class SubDepartment(BaseModel):
    id: str
    department_id: str
    link: str


class Program(BaseModel):
    id: str
    name: str
    sub_department_id: str
    department_id: str
    link: str


requests_get = lru_cache(maxsize=1000)(requests.get)


def scrape_departments() -> Iterable[Department]:
    departments_page = requests_get('https://academic.openu.ac.il/').content
    departments_page = BeautifulSoup(departments_page, 'html.parser')

    # Get the table containing the list of courses
    departments_containers = departments_page.select('.academic-department-promotion-box')
    departments_links = map(lambda department_container: department_container.select('a')[0], departments_containers)

    def get_department_from_link_tag(department_link_tag) -> Department:
        link = department_link_tag['href']
        image = department_link_tag.select('.img-responsive')[0]
        title = image['title']
        department_matches = re.findall(r'^https?:\/\/academic\.openu\.ac\.il\/(.*)$', link)
        if len(department_matches) != 1:
            raise AssertionError(f'Invalid department link format: {link}')
        department_id = department_matches[0]

        return Department(id=department_id, name=title)

    departments = map(get_department_from_link_tag, departments_links)

    return departments


def scrape_sub_departments(departments: Iterable[Department]) -> Iterable[SubDepartment]:
    departments_regx = '(' + '|'.join([department.id for department in departments]) + ')'
    sub_department_link_regx = rf'^https?:\/\/academic\.openu\.ac\.il\/{departments_regx}\/([^\/]*?)\/pages\/default\.aspx$'

    page = requests_get(r'https://www.openu.ac.il/').content
    page = BeautifulSoup(page, 'html.parser')

    a_tags = page.select('a')

    # Find all a tags in page linking to a sub department
    a_tags = filter(lambda a_tag: 'href' in a_tag.attrs, a_tags)
    links = map(lambda a_tag: a_tag['href'], a_tags)
    links = filter(lambda link: re.findall(
        sub_department_link_regx,
        link, re.IGNORECASE),
                   links)

    def get_sub_department(link: str) -> SubDepartment:
        matches = re.findall(sub_department_link_regx, link, re.IGNORECASE)
        if len(matches) != 1 or len(matches[0]) != 2:
            raise AssertionError(f'Unexpected link parsing result. link: {link}, matches: {matches}')
        department_id, sub_department_id = matches[0]

        return SubDepartment(id=sub_department_id, department_id=department_id, link=link)

    return map(get_sub_department, links)


def scrape_sub_department_programs(sub_department: SubDepartment) -> Iterable[Program]:
    print(f'Scraping programs of sub department {sub_department.id}')
    program_link_regx = rf'^https?:\/\/academic\.openu\.ac\.il\/{sub_department.department_id}\/{sub_department.id}\/program\/([^\/]*?)\.aspx$'

    page = requests_get(rf'https://academic.openu.ac.il/{sub_department.department_id}/{sub_department.id}/pages/programs.aspx').content
    page = BeautifulSoup(page, 'html.parser')

    a_tags = page.select('a')
    a_tags = filter(lambda a_tag: 'href' in a_tag.attrs, a_tags)
    links = map(lambda a_tag: a_tag['href'], a_tags)
    links = filter(lambda link: re.findall(program_link_regx, link, re.IGNORECASE),
                   links)

    def get_program(link: str) -> Program:
        matches = re.findall(program_link_regx, link, re.IGNORECASE)
        if len(matches) != 1:
            raise AssertionError(f'Unexpected link parsing result. link: {link}, matches: {matches}')
        program_id = matches[0]

        program_page = requests_get(rf'https://academic.openu.ac.il/{sub_department.department_id}/{sub_department.id}/program/{program_id}.aspx').content
        program_page = BeautifulSoup(program_page, 'html.parser')
        title = program_page.select_one(r'p[class=Heading1]')
        title = title.text.strip() if title else program_id

        return Program(id=program_id, name=title, sub_department_id=sub_department.id, department_id=sub_department.department_id, link=link)

    return map(get_program, links)


def scrape_program_courses_ids(program: Program) -> Iterable[str]:
    # print(f'Scraping courses of program {program.id}')
    course_link_regx = r'^https?:\/\/www3\.openu\.ac\.il\/ouweb\/owa\/yed\.daf_kurs\?mid=\d+&mkurs=(\d+)$'

    page = requests_get(f'https://academic.openu.ac.il/{program.department_id}/{program.sub_department_id}/program/{program.id}.aspx').content
    page = BeautifulSoup(page, 'html.parser')

    a_tags = page.select('a')
    a_tags = filter(lambda a_tag: 'href' in a_tag.attrs, a_tags)
    links = map(lambda a_tag: a_tag['href'], a_tags)
    links = filter(lambda link: re.findall(course_link_regx, link, re.IGNORECASE),
                   links)

    def get_course_id(link: str) -> str:
        matches = re.findall(course_link_regx, link, re.IGNORECASE)
        if len(matches) != 1:
            raise AssertionError(f'Unexpected link parsing result. link: {link}, matches: {matches}')
        course_id = matches[0]

        return course_id

    return map(get_course_id, links)


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)

    print('Scraping departments')
    departments = scrape_departments()

    print('Scraping sub departments')
    sub_departments = scrape_sub_departments(departments)

    print('Scraping programs')
    pool = Pool(processes=100)
    programs = pool.map(scrape_sub_department_programs, sub_departments)
    print('Reducting programs')
    programs = reduce(lambda programs_a, programs_b: chain(programs_a, programs_b), programs)

    print('Iterating over programs')
    for program in programs:
        courses_ids = list(scrape_program_courses_ids(program))
        print(f'{program.department_id} - {program.sub_department_id} - {program.id} - {len(courses_ids)} courses')
