#!/bin/sh

# This script assumes that the current working directory is the project root dir

echo "installing npm packages"
npm install --only=dev --only=prod

echo "running react server"
set CI=true
npm run start --host 0.0.0.0

echo "Done with hot reload script, exiting... (if this you see this line the npm run start script stopped for some reason)"
