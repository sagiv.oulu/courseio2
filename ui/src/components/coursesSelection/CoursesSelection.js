import React from "react";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import MailIcon from "@material-ui/icons/Mail";
import FormLabel from "@material-ui/core/FormLabel";
import FormControl from "@material-ui/core/FormControl";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import FormHelperText from "@material-ui/core/FormHelperText";
import Checkbox from "@material-ui/core/Checkbox";

const useStyles = makeStyles((theme) => ({}));

function CoursesSelection() {
  const classes = useStyles();
  const theme = useTheme();

  return (
    <div>
      <FormControl component="fieldset">
        <FormLabel component="legend">קורסים שסיימתי</FormLabel>
        <FormGroup>
          <FormControlLabel control={<Checkbox />} label="אינפי 1" />
          <FormControlLabel control={<Checkbox />} label="אינפי 2" />
          <FormControlLabel control={<Checkbox />} label="לוגיקה" />
        </FormGroup>
        <FormHelperText>Be careful</FormHelperText>
      </FormControl>
      {/* <List>
        {["אינפי 1", "אינפי 2", "לוגיקה", "אלגוריתמים"].map((text, index) => (
          <ListItem checkbox key={text}>
            <ListItemText primary={text} />
            <ListItemIcon>
              {index % 2 === 0 ? <InboxIcon /> : <MailIcon />}
            </ListItemIcon>
          </ListItem>
        ))}
      </List> */}
    </div>
  );
}

export default CoursesSelection;
