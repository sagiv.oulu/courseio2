import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Board from "@lourenci/react-kanban";

const useStyles = makeStyles((theme) => ({}));

export default function CoursesKanban() {
  const classes = useStyles();
  const board = {
    columns: [
      {
        id: 1,
        title: "Backlog",
        cards: [
          {
            id: 1,
            title: "Add card",
            description: "Add capability to add a card in a column",
          },
        ],
      },
      {
        id: 2,
        title: "Doing",
        cards: [
          {
            id: 2,
            title: "Drag-n-drop support",
            description: "Move a card between the columns",
          },
        ],
      },
    ],
  };
  return <Board initialBoard={board} />;
}
