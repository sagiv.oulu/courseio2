import React, { useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Board from "@lourenci/react-kanban";
import StudiesContext from "../../contexts/StudiesContext";

const useStyles = makeStyles((theme) => ({}));

export default function SingleSemester({ semesterName }) {
  const { studies } = useContext(StudiesContext);

  const getSemesterBoard = (semesterName) => {
    let board = {
      columns: [
        {
          id: `${semesterName}-possible`,
          title: `${semesterName} Possible courses`,
          cards: [],
        },
        {
          id: `${semesterName}-selected`,
          title: `${semesterName} Selected courses`,
          cards: [],
        },
      ],
    };

    return board;
  };

  const classes = useStyles();
  return <Board disableColumnDrag>{getSemesterBoard(semesterName)}</Board>;
}
