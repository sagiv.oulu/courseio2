import React, { createContext, useState, useEffect } from "react";
import PropTypes from "prop-types";
import _, { trim } from "lodash";
import BooleanExpressions from "boolean-expressions";

export class Course {
  id: number;
  name: string;
  points: number;
  subjects: Array<number>;
  association: string;
  requirements: string;
  requirementsExpression: BooleanExpressions;

  constructor(
    id: number,
    name: string,
    points: number,
    subjects: Array<number>,
    association: string,
    requirements: string
  ) {
    this.id = id;
    this.name = name;
    this.points = points;
    this.subjects = subjects;
    this.association = association;
    this.requirements = requirements;
    if (
      /(=|>|<)/.test(this.requirements) ||
      /subject/.test(this.requirements)
    ) {
      this.requirementsExpression = new BooleanExpressions("false");
    } else {
      this.requirementsExpression = new BooleanExpressions(
        requirements.replaceAll("_", "")
      );
    }
  }

  areRequirementsMet(completedCourses: Array<Course>): boolean {
    console.debug(`Checking reqs of course ${this.id}`);
    // // For now I am not able to parse boolean expressions with (x = y) expressions ( or <, > operators).
    // //  so evaluate every requirements with this expression as false FOR NOW.
    // //  TODO: Evaluate "=/</>" expressions and support points/X_points values in boolean expression (for example: cours_1234 and science_points > 3)
    // if ((/(=|>|<)/).test(this.requirements)) {
    //   return false;
    // }

    // // For now I dont support expressions with course subjects, such as: (course_1234 and two_of(subject_9876))
    // // TODO: Support these expressions
    // if ((/subject/).test(this.requirements)) {
    //   return false;
    // }

    // const requirements = new BooleanExpressions(this.requirements.replaceAll('_', ''));
    // const areRequirementsMet = requirements.evaluate(completedCourses.map(course => `course${course.id}`));
    // return areRequirementsMet;

    return this.requirementsExpression.evaluate(
      completedCourses.map((course) => `course${course.id}`)
    );
  }
}

const getSemesterName = (year: number, trimesterIndex: number): string => {
  const trimesterName = ["A", "B", "C"][trimesterIndex];
  return `${year} ${trimesterName}`;
};

export class Semester {
  year: number;

  // The index of the semester in the year, possible values: 0, 1, 2
  trimesterIndex: number;

  studies: Studies;
  selectedCourses: Array<Course>;

  constructor(
    year: number,
    trimesterIndex: number,
    studies: Studies,
    selectedCourses: Array<Course> = []
  ) {
    this.year = parseInt(year.toString()); // year;
    this.trimesterIndex = parseInt(trimesterIndex.toString()); //trimesterIndex;
    this.studies = studies;
    this.selectedCourses = selectedCourses;
  }

  getName(): string {
    return getSemesterName(this.year, this.trimesterIndex);
  }

  getCompletedCourses(): Array<Course> {
    // Return all selected courses from previous semesters in studies

    let previousSemesters: Array<Semester> = this.studies.semesters.filter(
      (semester) =>
        semester.year <= this.year &&
        semester.trimesterIndex < this.trimesterIndex
    );

    let completedCourses: Array<Course> = previousSemesters.reduce(
      (courses, semester) => courses.concat(semester.selectedCourses),
      [] as Array<Course>
    );

    return completedCourses;
  }

  getPossibleCourses() {
    let completedCourses: Array<Course> = this.getCompletedCourses();
    console.log(`Semester ${this.getName()} completedCourses`);
    console.log({ completedCourses });

    // Get all uncompleted courses
    let uncompletedCourses: Array<Course> = _.difference(
      this.studies.allCourses,
      completedCourses
    );

    console.log({ uncompletedCourses });

    // check each uncompleted course if it's requirements are met
    let possibleCourses: Array<Course> = uncompletedCourses.filter((course) =>
      course.areRequirementsMet(completedCourses)
    );

    return possibleCourses;
  }

  getPossibleUnselectedCourses() {
    const selectedCoursesIds = this.selectedCourses.map((course) => course.id);
    const courses = this.getPossibleCourses().filter(
      (course) => selectedCoursesIds.indexOf(course.id) === -1
    );

    return courses;
  }

  removeImpossibleCourses(): Semester {
    console.log(`Removing impossible courses in semester ${this.getName()}`);

    const possibleCourses = this.getPossibleCourses();
    console.log({ possibleCourses });

    // Get all selected courses that are not possible in this semester
    let impossibleSelectedCourses: Array<Course> = _.difference(
      this.selectedCourses,
      possibleCourses
    );
    console.log(
      `Semester ${this.getName()} has ${
        impossibleSelectedCourses.length
      } impossible but selected courses`
    );

    // Keep only selected courses that are not impossible
    let fixedSelectedCourses: Array<Course> = this.selectedCourses.filter(
      (course) => !impossibleSelectedCourses.includes(course)
    );

    return new Semester(
      this.year,
      this.trimesterIndex,
      this.studies,
      fixedSelectedCourses
    );
  }
}

export class Studies {
  semesters: Array<Semester>;
  allCourses: Array<Course>;

  constructor(semesters: Array<Semester>, allCourses: Array<Course>) {
    this.semesters = semesters.map((s) => {
      s.studies = this;
      return s;
    });
    this.allCourses = allCourses;
  }

  getSotredSemesters(): Array<Semester> {
    return this.semesters.sort(
      (semA, semB) =>
        semA.year * 10 +
        semA.trimesterIndex -
        (semB.year * 10 + semB.trimesterIndex)
    );
  }

  getSemester(year: number, trimesterIndex: number): Semester {
    let matchingSemesters = this.semesters.filter(
      (semester) =>
        semester.year === year && semester.trimesterIndex === trimesterIndex
    );
    let semester = null;
    if (matchingSemesters.length === 1) {
      const matchingSemester = matchingSemesters[0];
      semester = new Semester(
        matchingSemester.year,
        matchingSemester.trimesterIndex,
        this,
        matchingSemester.selectedCourses
      );
    } else if (matchingSemesters.length === 0) {
      semester = new Semester(year, trimesterIndex, this, []);
    } else {
      throw `Unexpected amount of semesters matching the year ${year} and trimester index ${trimesterIndex}. found ${matchingSemesters.length} matches`;
    }

    return semester;
  }

  removeImpossibleCourses(): Studies {
    // Fix selected courses in each semester, from first semester to last
    let newStudies = new Studies(this.getSotredSemesters(), this.allCourses);
    for (let index = 0; index < newStudies.semesters.length; index++) {
      newStudies.semesters[index] = newStudies.semesters[
        index
      ].removeImpossibleCourses();
    }

    return newStudies;
  }
}

export const StudiesContext = createContext({ studies: null });

export default function StudiesContextProvider({ children }: any) {
  const [studies, setStudies] = useState<Studies>(new Studies([], []));

  const addSemester = (
    year: number,
    trimesterIndex: number,
    semestersToAdd: number
  ) => {
    let newYear = year;
    let newTrimesterIndex = trimesterIndex + semestersToAdd;
    newYear = newYear + Math.floor(newTrimesterIndex / 3);
    newTrimesterIndex = newTrimesterIndex % 3;
    if (newTrimesterIndex < 0) {
      newYear -= 1;
      newTrimesterIndex += 3;
    }
    return [newYear, newTrimesterIndex];
  };

  const selectCourse = (semester: Semester, course: Course) => {
    setStudies((prevStudies) => {
      let newSemester = prevStudies.getSemester(
        semester.year,
        semester.trimesterIndex
      );
      newSemester.selectedCourses.push(course);

      let newSemesters = prevStudies.semesters.filter(
        (semester) =>
          semester.year != newSemester.year ||
          semester.trimesterIndex != newSemester.trimesterIndex
      );
      newSemesters.push(newSemester);

      let newStudies = new Studies(newSemesters, prevStudies.allCourses);
      newStudies = newStudies.removeImpossibleCourses();
      return newStudies;
    });
  };

  const unselectCourse = (semester: Semester, course: Course) => {
    setStudies((prevStudies) => {
      let newSemester = prevStudies.getSemester(
        semester.year,
        semester.trimesterIndex
      );
      newSemester.selectedCourses = newSemester.selectedCourses.filter(
        (c) => c.id !== course.id
      );

      let newSemesters = prevStudies.semesters.filter(
        (semester) =>
          semester.year != newSemester.year ||
          semester.trimesterIndex != newSemester.trimesterIndex
      );
      newSemesters.push(newSemester);

      let newStudies = new Studies(newSemesters, prevStudies.allCourses);
      newStudies = newStudies.removeImpossibleCourses();
      return newStudies;
    });
  };

  // Load all courses for the first time
  useEffect(() => {
    fetch("/programs/AF/courses")
      .then((res) => res.json())
      // .then((coursesJsons) =>
      //   coursesJsons.filter((course: any) => /אינפי/.test(course.name))
      // )
      .then((coursesJsons) =>
        coursesJsons.map(
          (courseJson: any) =>
            new Course(
              courseJson.id,
              courseJson.name,
              courseJson.points,
              courseJson.subjects,
              courseJson.association,
              courseJson.requirements
            )
        )
      )
      .then((allCourses: Array<Course>) => {
        console.debug(`Loaded all courses (${allCourses.length}) from api`);
        // let courses = allCourses.filter(course => (/מחשב/).test(course.association));
        setStudies(new Studies([], allCourses));
      });
  }, []);

  return (
    <StudiesContext.Provider
      value={
        {
          studies,
          setStudies,
          selectCourse,
          unselectCourse,
          addSemester,
          getSemesterName,
        } as any
      }
    >
      {children}
    </StudiesContext.Provider>
  );
}

StudiesContextProvider.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.node),
    PropTypes.node,
  ]).isRequired,
};
