import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import TopBar from "./containers/TopBar/TopBar";
import Main from "./containers/Main/Main";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
}));

export default function App() {
  const classes = useStyles();
  const [semester, setSemester] = useState();
  return (
    <div className={classes.root}>
      <TopBar></TopBar>
      <Main></Main>
    </div>
  );
}
