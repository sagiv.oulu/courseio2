import React, { useState, useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemSecondaryAction from "@material-ui/core/ListItemSecondaryAction";
import ListItemText from "@material-ui/core/ListItemText";
import Avatar from "@material-ui/core/Avatar";
import IconButton from "@material-ui/core/IconButton";
import FolderIcon from "@material-ui/icons/Folder";
import DeleteIcon from "@material-ui/icons/Delete";
import Typography from "@material-ui/core/Typography";
import ListSubheader from "@material-ui/core/ListSubheader";
import DroppableCoursesList from "../DroppableCoursesList/DroppableCoursesList";
import { StudiesContext } from "../../contexts/StudiesContext";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";
import CourseListItem from "../CourseListItem/CourseListItem";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  list: {
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    height: "200px",
    overflow: "auto",
  },
}));

export default function SemesterBoard({ year, trimesterIndex }) {
  const classes = useStyles();

  const {
    studies,
    selectCourse,
    unselectCourse,
    addSemester,
    getSemesterName,
  } = useContext(StudiesContext);

  const [semester, setSemester] = useState(null);
  const [possibleCourses, setPossibleCourses] = useState([]);
  const [selectedCourses, setSelectedCourses] = useState([]);

  useEffect(() => {
    const semester = studies.getSemester(year, trimesterIndex);
    setSemester(semester);

    let newPossibleCourses = semester.getPossibleUnselectedCourses();
    setPossibleCourses(newPossibleCourses);

    let newSelectedCourses = semester.selectedCourses;
    setSelectedCourses(newSelectedCourses);
  }, [studies, year, trimesterIndex]);

  return (
    <div className={classes.root}>
      <Paper variant="outlined" className={classes.list}>
        <List dense>
          <ListSubheader>
            <Typography variant="h6" color="inherit">
              {`Selected ${selectedCourses.length} ${
                selectedCourses.length === 1 ? "course" : "courses"
              }`}
            </Typography>
          </ListSubheader>
          {selectedCourses.map((course) => (
            <CourseListItem
              key={`${course.id}`}
              course={course}
              selected={true}
              onClick={(course) => unselectCourse(semester, course)}
            ></CourseListItem>
          ))}
        </List>
      </Paper>
      <Paper variant="outlined" className={classes.list}>
        <List dense>
          <ListSubheader>
            <Typography variant="h6" color="inherit">
              {`${possibleCourses.length} possible ${
                possibleCourses.length === 1 ? "course" : "courses"
              }`}
            </Typography>
          </ListSubheader>
          {possibleCourses.map((course) => (
            <CourseListItem
              key={`${course.id}`}
              course={course}
              selected={false}
              onClick={(course) => selectCourse(semester, course)}
            ></CourseListItem>
          ))}
        </List>
      </Paper>
    </div>
  );
}
