import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";

import clsx from "clsx";
import { DragDropContext, Droppable, Draggable } from "react-beautiful-dnd";

const useStyles = makeStyles((theme) => ({
  droppableContainer: {
    padding: theme.spacing(1),
    margin: theme.spacing(1),
    // minHeight: "200px",
  },
  droppableRoot: {
    minHeight: "100px",
  },
  draggableContainer: {
    padding: theme.spacing(1),
    margin: theme.spacing(1),
  },
  title: {
    textAlign: "center",
  },
}));

export default function DroppableCoursesList({ courses, title, droppableId }) {
  const classes = useStyles();

  return (
    <Paper variant="outlined" className={classes.droppableContainer}>
      <Typography variant="h6" color="inherit" className={classes.title}>
        {title} - {courses.length} courses
      </Typography>
      <Divider></Divider>
      <Droppable droppableId={droppableId} className={classes.droppable}>
        {(provided, snapshot) => (
          <div
            {...provided.droppableProps}
            ref={provided.innerRef}
            className={classes.droppableRoot}
          >
            {courses.map((course, index) => (
              <Draggable
                key={course.id}
                draggableId={`${droppableId}-${course.id}`}
                index={index}
              >
                {(provided, snapshot) => (
                  <Paper
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                    variant="outlined"
                    className={classes.draggableContainer}
                  >
                    {course.name} ({course.id})
                  </Paper>
                )}
              </Draggable>
            ))}
          </div>
        )}
      </Droppable>
    </Paper>
  );
}
