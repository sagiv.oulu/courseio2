import React, { useState, useEffect, useContext } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import IconButton from "@material-ui/core/IconButton";
import ArrowLeftIcon from "@material-ui/icons/ArrowLeft";
import ArrowRightIcon from "@material-ui/icons/ArrowRight";
import Typography from "@material-ui/core/Typography";
import { DragDropContext } from "react-beautiful-dnd";

import SemesterBoard from "../SemesterBoard/SemesterBoard";
import { StudiesContext, Studies } from "../../contexts/StudiesContext";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(1),
  },
}));

export default function SemestersBoard() {
  const classes = useStyles();

  const { studies, setStudies, addSemester, getSemesterName } = useContext(
    StudiesContext
  );

  const [startYear, setStartYear] = useState(2020);
  const [startTrimesterIndex, setStartTrimesterIndex] = useState(0);

  const trimestersToShow = 3;

  const handleRightClick = () => {
    const [newYear, newStartTrimesterIndex] = addSemester(
      startYear,
      startTrimesterIndex,
      1
    );
    setStartYear(newYear);
    setStartTrimesterIndex(newStartTrimesterIndex);
  };

  const handleLeftClick = () => {
    const [newYear, newStartTrimesterIndex] = addSemester(
      startYear,
      startTrimesterIndex,
      -1
    );
    setStartYear(newYear);
    setStartTrimesterIndex(newStartTrimesterIndex);
  };

  const getDragInformation = (draggableId, source, destination) => {
    const sourceDroppableId = source.droppableId;

    // Get the course that was dragged
    const courseId = draggableId
      .replaceAll(sourceDroppableId, "")
      .replaceAll("-", "");
    const course = studies.allCourses.find((c) => c.id == courseId);

    // Get the destination of the drag (year, trimester_index & wether it was dragged to a possible list of selected list)
    const destinationMatches = destination.droppableId.match(
      /(\d+)-(\d)-(selected|possible).*/
    );
    if (destinationMatches.length !== 4) {
      throw `Unexpected destination droppable id: ${destination.droppableId}`;
    }
    const destinationYear = parseInt(destinationMatches[1]);
    const destinationTrimesterIndex = parseInt(destinationMatches[2]);
    const destinationListType = destinationMatches[3];
    const destinationSemester = studies.getSemester(
      destinationYear,
      destinationTrimesterIndex
    );

    // Get the source of the drag (year, trimester_index & wether it was dragged to a possible list of selected list)
    const sourceMatches = source.droppableId.match(
      /(\d+)-(\d)-(selected|possible).*/
    );
    if (sourceMatches.length !== 4) {
      throw `Unexpected destination droppable id: ${source.droppableId}`;
    }
    const sourceYear = parseInt(sourceMatches[1]);
    const sourceTrimesterIndex = parseInt(sourceMatches[2]);
    const sourceListType = sourceMatches[3];
    const sourceSemester = studies.getSemester(
      sourceYear,
      sourceTrimesterIndex
    );

    return {
      course,
      sourceSemester,
      sourceListType,
      destinationSemester,
      destinationListType,
    };
  };

  const onDragEnd = ({ draggableId, source, destination }) => {
    const {
      course,
      sourceSemester,
      sourceListType,
      destinationSemester,
      destinationListType,
    } = getDragInformation(draggableId, source, destination);

    console.debug(
      `Dragged ${
        course.id
      } to semester ${destinationSemester.getName()} ${destinationListType}`
    );

    // If the course was draged from a possible list to a selected list
    if (sourceListType === "possible" && destinationListType === "selected") {
      console.log(
        `Before the move, the semester had ${destinationSemester.selectedCourses.length} selected courses`
      );
      destinationSemester.selectedCourses.push(course);
      console.log(
        `The new semester has now ${destinationSemester.selectedCourses.length} selected courses`
      );
      console.log({ newSemester: destinationSemester });

      console.log(
        "Creating a new semesters list, and replacing the old semester with the new one"
      );
      let newSemesters = studies.semesters.filter(
        (semester) =>
          semester.year != destinationSemester.year ||
          semester.trimesterIndex != destinationSemester.trimesterIndex
      );
      newSemesters.push(destinationSemester);

      console.log(
        "Creating a new studies object & setting it the the studies state"
      );
      let newStudies = new Studies(newSemesters, studies.allCourses);
      console.log({ newStudies: newStudies });

      let forTestingNewSemester = newStudies.getSemester(
        destinationSemester.year,
        destinationSemester.trimesterIndex
      );
      console.log(
        `After getting the new course again from new studies, it has ${forTestingNewSemester.selectedCourses.length} selected course`
      );

      setStudies(newStudies);
    }
  };

  return (
    <div className={classes.root}>
      <DragDropContext onDragEnd={onDragEnd}>
        <Grid container>
          <Grid item container xs={12} justify="center">
            <Grid
              item
              xs={1}
              justifyContent="center"
              style={{ textAlign: "center" }}
            >
              <IconButton
                edge="start"
                color="inherit"
                aria-label="leftArrow"
                onClick={handleLeftClick}
              >
                <ArrowLeftIcon />
              </IconButton>
            </Grid>
            <Grid
              item
              xs={1}
              alignItems="center"
              style={{ textAlign: "center" }}
            >
              <Typography variant="h6" color="inherit">
                {getSemesterName(startYear, startTrimesterIndex)} -{" "}
                {(() => {
                  const [endYear, endTrimesterIndex] = addSemester(
                    startYear,
                    startTrimesterIndex,
                    trimestersToShow - 1
                  );

                  return getSemesterName(endYear, endTrimesterIndex);
                })()}
              </Typography>
            </Grid>
            <Grid
              item
              xs={1}
              alignItems="center"
              style={{ textAlign: "center" }}
            >
              <IconButton
                edge="start"
                color="inherit"
                aria-label="rightArrow"
                onClick={handleRightClick}
              >
                <ArrowRightIcon />
              </IconButton>
            </Grid>
          </Grid>
          <Grid item container xs={12}>
            {Array(trimestersToShow)
              .fill(0)
              .map((item, index, array) => {
                const spacing = Math.floor(12 / array.length);
                const [year, trimesterIndex] = addSemester(
                  startYear,
                  startTrimesterIndex,
                  index
                );
                return (
                  <Grid item xs={spacing}>
                    <SemesterBoard
                      year={year}
                      trimesterIndex={trimesterIndex}
                    />
                  </Grid>
                );
              })}
          </Grid>
        </Grid>
      </DragDropContext>
    </div>
  );
}
