import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";
import Grid from "@material-ui/core/Grid";

import StudiesContextProvider from "../../contexts/StudiesContext";
import SemestersBoard from "../SemestersBoard/SemestersBoard";

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    padding: theme.spacing(1),
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  semesterControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
}));

export default function Main() {
  const [degreeType, setDegreeType] = useState("computerScience");
  const [degreeTypes, setDegreeTypes] = useState([
    {
      key: "computerScience",
      label: "חוג במדעי המחשב לתואר דו-חוגי",
    },
    {
      key: "computerEngineering",
      label: "בוגר (‏﻿B.Sc.‎)‏ בהנדסת תוכנה",
    },
  ]);

  const classes = useStyles();

  return (
    <StudiesContextProvider>
      <div className={classes.root}>
        <Grid container spacing={2}>
          <Grid item xs={12}>
            <FormControl variant="outlined" className={classes.semesterControl}>
              <InputLabel id="degree-type">Degree type</InputLabel>
              <Select
                labelId="degree-type"
                id="demo-simple-select-outlined"
                value={degreeType}
                onChange={(event) => setDegreeType(event.target.value)}
                label="Degree type"
              >
                {degreeTypes.map((degreeType) => (
                  <MenuItem value={degreeType.key} key={degreeType.key}>
                    {degreeType.label}
                  </MenuItem>
                ))}
              </Select>
            </FormControl>
          </Grid>
          <Grid item xs={12}>
            <SemestersBoard></SemestersBoard>
          </Grid>
        </Grid>
      </div>
    </StudiesContextProvider>
  );
}
